#ifndef CSECEXT_GLOBAL_H
#define CSECEXT_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(CSECEXT_LIBRARY)
#  define CSECEXTSHARED_EXPORT Q_DECL_EXPORT
#else
#  define CSECEXTSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // CSECEXT_GLOBAL_H
